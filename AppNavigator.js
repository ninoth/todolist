import { createStackNavigator } from 'react-navigation';
import Home from './src/home';
import Todolist from './src/todolist'
import Loaddata from './src/loaddata'

const AppNavigator = createStackNavigator({
  Home: { screen: Home },
  Todolist: { screen: Todolist },
  Loaddata: { screen: Loaddata}
});

export default AppNavigator;


