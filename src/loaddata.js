import React from 'react';
import { StyleSheet, Text, View, Image,TextInput,FlatList, ActivityIndicator} from 'react-native';
import { Item } from 'react-native/Libraries/Components/Picker/Picker';

//MAIN
export default class Loaddata extends React.Component{

  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }

  componentDidMount(){
    return fetch('https://devapi.fore.coffee/testing')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson.payload.data,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }


  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 50}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={{flex: 1, paddingTop:50, backgroundColor: '#fff'}}>
      <Text style={styles.title}>From: https://devapi.fore.coffee/testing</Text>
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) => 
                                  <Text style={styles.body}>Id: {item.test_id}{"\n"} 
                                        Test_name: {item.test_name}{"\n"}
                                        Test_type: {item.test_type}{"\n"}
                                        Created_date: {item.created_date}
                                  </Text>}
          keyExtractor={({id}, index) => id}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title:{
    padding: 10,
    fontWeight: 'bold'
  },
  body:{
    padding: 15,
  }
});

