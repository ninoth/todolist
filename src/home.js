import React, {Component} from 'react';
import {Platform, TouchableHighlight, Button, StyleSheet, Text, View, ActivityIndicator,FlatList} from 'react-native';
import { Item } from 'react-native/Libraries/Components/Picker/Picker';

// type Props = {};
export default class Home extends React.Component {

  render() {
      return(
        <View style={styles.container}>
          <Text style={styles.header}>
           My To Do List (
           { this.props.screenProps.currentList.length }
           )         
          </Text>
          {
            this.props.screenProps.currentList.map((todo, index) => (
              <Button
                key={ todo }
                title={ `o ${ todo }` }
              />
            ))
          }

          <TouchableHighlight 
                style ={styles.button}>
          <Button
            title="+ Add to do list"
            style={styles.button}
            onPress={()=> this.props.navigation.navigate('Todolist')}
          />
          </TouchableHighlight>

          <TouchableHighlight 
                style ={styles.button2}>
          <Button
            title="API Testing"
            style={styles.button}
            onPress={()=> this.props.navigation.navigate('Loaddata')}
          />
          </TouchableHighlight>

        </View>
        
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header:{
    fontSize: 25
  },
  button: {
    height: 40,
    width:160,
    borderRadius:10,
    backgroundColor : "yellow",
    marginLeft :50,
    marginRight:50,
    marginTop :20
  },
  button2: {
    height: 40,
    width:160,
    borderRadius:10,
    backgroundColor : "cyan",
    marginLeft :50,
    marginRight:50,
    marginTop :20
  }
});
