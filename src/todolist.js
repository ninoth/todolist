import React, {Component} from 'react';
import {Platform, Button, TouchableHighlight, StyleSheet, Text, View, ActivityIndicator,FlatList} from 'react-native';
import { Item } from 'react-native/Libraries/Components/Picker/Picker';

// type Props = {};
export default class Todolist extends React.Component {

  render() {
      return(
        <View style={styles.container}>
          <Text style={styles.header}>
           Recent To Do List
           </Text>
           {
            this.props.screenProps.myList.map((todo, index) => (
              <Button
                key={ todo }
                title={ `+ ${ todo }` }
                onPress={() =>
                  this.props.screenProps.addList(index)
                }
              />
            ))
          }
        
          <TouchableHighlight style ={styles.button}>
          <Button
            title="Back"
            style={styles.button}
            onPress={()=> this.props.navigation.navigate('Home')}
          />
          </TouchableHighlight>

        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header:{
    fontSize: 25
  },
  button: {
    height: 40,
    width:160,
    borderRadius:10,
    backgroundColor : "orange",
    marginLeft :50,
    marginRight:50,
    marginTop :20
  }
});
