import React from 'react';
import AppNavigator from './AppNavigator';

export default class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      myList:[
        'Inisiasi Project',
        'Routing App',
        'Handle API',
        'State Management'
      ],
      currentList:[
      
      ]
    }
  }

  addList = (index) => {
    const {
      currentList,
      myList
    } = this.state

    const addedList = myList.splice(index,1)
    currentList.push(addedList);

    this.setState({
      currentList,
      myList
    })
  }  

  render() {
    return (
      <AppNavigator
        screenProps={
          {
            currentList : this.state.currentList,
            myList: this.state.myList,
            addList: this.addList
          }
        }

      />
    );
  }
}